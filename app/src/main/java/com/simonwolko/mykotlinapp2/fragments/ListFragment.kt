package com.simonwolko.mykotlinapp2.fragments

import android.app.Fragment
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.simonwolko.mykotlinapp2.R
import com.simonwolko.mykotlinapp2.adapters.ListeAdapter
import com.simonwolko.mykotlinapp2.classes.AndroidVersion

class ListFragment : Fragment() {
    // UI
    var recyclerView:RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Récupération des objets
        val items: ArrayList<AndroidVersion> = ArrayList()
        items.add(AndroidVersion("Honeycomb", "3.0", BitmapFactory.decodeResource(resources, R.drawable.honeycomb)))
        items.add(AndroidVersion("Ice Cream Sandwich", "4.0", BitmapFactory.decodeResource(resources, R.drawable.icecreamsandwich)))
        items.add(AndroidVersion("Jelly Bean", "4.1", BitmapFactory.decodeResource(resources, R.drawable.jellybean)))
        items.add(AndroidVersion("KitKat", "4.4", BitmapFactory.decodeResource(resources, R.drawable.kitkat)))
        items.add(AndroidVersion("Lollipop", "5.1", BitmapFactory.decodeResource(resources, R.drawable.lollipop)))
        items.add(AndroidVersion("Marshmallow", "6.0", BitmapFactory.decodeResource(resources, R.drawable.marshmallow)))
        items.add(AndroidVersion("Nougat", "7.0", BitmapFactory.decodeResource(resources, R.drawable.nougat)))

        // Récupération des éléments
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView?.adapter = ListeAdapter(items)
        recyclerView?.layoutManager = LinearLayoutManager(activity)
    }
}

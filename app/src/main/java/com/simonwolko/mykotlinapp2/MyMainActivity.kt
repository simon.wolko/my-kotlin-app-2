package com.simonwolko.mykotlinapp2

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.antonioleiva.weatherapp.extensions.DelegatesExt
import com.simonwolko.mykotlinapp2.fragments.GalleryFragment
import com.simonwolko.mykotlinapp2.fragments.ImportFragment
import com.simonwolko.mykotlinapp2.fragments.ListFragment
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.yesButton

class MyMainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    // NF
    private var name: String by DelegatesExt.preference(this, MySettingsActivity.NAME, MySettingsActivity.DEFAULT_NAME)
    private var color: String by DelegatesExt.preference(this, MySettingsActivity.COLOR, MySettingsActivity.DEFAULT_COLOR)
    private var activeFragmentName: String? = null
    // UI
    private var toolbar:Toolbar? = null
    private var textViewName: TextView? = null
    private var textViewEmail: TextView? = null
    private var headerView: View? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_main)

        // Get active fragment
        activeFragmentName = savedInstanceState?.getString("ACTIVE_FRAGMENT_NAME")
        if (activeFragmentName == null) activeFragmentName = "NAV_CAMERA"

        // Toolbar
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        // Refresh colors
        refreshColors()

        // Restore fragment state
        restoreFragmentState(activeFragmentName)

        // Init navigation
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.setDrawerListener(toggle)
        toggle.syncState()
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)

        // Navigation Header
        headerView = navigationView.getHeaderView(0)
        textViewName = headerView?.findViewById(R.id.textViewNom)
        textViewEmail = headerView?.findViewById(R.id.textViewEmail)
    }

    override fun onResume() {
        super.onResume()

        // Refresh colors
        refreshColors()

        // Navigation Header
        textViewName?.text = name
        textViewEmail?.text = name.trim() + "@example.com"
    }

    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.my_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        return if (id == R.id.action_settings) {
            alert("Personalisez l'application", "Réglages") {
                yesButton {
                    startActivity<MySettingsActivity>()
                }
                noButton {}
            }.show()

            true
        } else super.onOptionsItemSelected(item)

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        val ft = fragmentManager.beginTransaction()

        when (id) {
            R.id.nav_camera -> {

                ft.replace(R.id.listFragment, ImportFragment())
                ft.commit()
                activeFragmentName = "NAV_CAMERA"

            }
            R.id.nav_gallery -> {

                ft.replace(R.id.listFragment, GalleryFragment())
                ft.commit()
                activeFragmentName = "NAV_GALLERY"

            }
            R.id.nav_list -> {

                ft.replace(R.id.listFragment, ListFragment())
                ft.commit()
                activeFragmentName = "NAV_LIST"

            }
            R.id.nav_send -> {

                startActivity<MySecondActivity>()

            }
        }

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    private fun refreshColors() {
        val intColor: Int = Color.parseColor(color)

        // ToolBar
        toolbar?.setBackgroundColor(Color.parseColor(color))

        // Status Bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) window.statusBarColor = intColor

        // Navigation header
        headerView?.setBackgroundColor(intColor)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putString("ACTIVE_FRAGMENT_NAME", activeFragmentName)

        super.onSaveInstanceState(outState)
    }

    private fun restoreFragmentState(menu: String?) {
        val ft = fragmentManager.beginTransaction()
        when (menu) {
            "NAV_CAMERA" -> {
                ft.replace(R.id.listFragment, ImportFragment())
            }
            "NAV_GALLERY" -> {
                ft.replace(R.id.listFragment, GalleryFragment())
            }
            "NAV_LIST" -> {
                ft.replace(R.id.listFragment, ListFragment())
            }
        }
        ft.commit()
    }
}

package com.simonwolko.mykotlinapp2.classes

import android.graphics.Bitmap

class AndroidVersion (val nom: String, val version: String, val logo: Bitmap)

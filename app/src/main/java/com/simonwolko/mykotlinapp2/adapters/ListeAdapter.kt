package com.simonwolko.mykotlinapp2.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.view.LayoutInflater
import android.widget.ImageView
import com.simonwolko.mykotlinapp2.R
import com.simonwolko.mykotlinapp2.classes.AndroidVersion

class ListeAdapter(val items: ArrayList<AndroidVersion>) : RecyclerView.Adapter<ListeAdapter.ViewHolder>() {

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items.get(position)
        holder.display(item)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val image: ImageView
        private val nom: TextView
        private val version: TextView

        private var currentItem: AndroidVersion? = null

        init {
            image = itemView.findViewById(R.id.logo)
            nom = itemView.findViewById(R.id.nom)
            version = itemView.findViewById(R.id.version)
        }

        fun display(item: AndroidVersion) {
            currentItem = item

            image.setImageBitmap(item.logo)
            nom.text = item.nom
            version.text = "Version : " + item.version
        }
    }

}

package com.simonwolko.mykotlinapp2

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.EditText
import android.widget.SeekBar
import android.widget.Switch
import com.antonioleiva.weatherapp.extensions.DelegatesExt
import com.jaredrummler.android.colorpicker.ColorPickerDialog
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener

class MySettingsActivity : AppCompatActivity(), ColorPickerDialogListener {

    companion object {
        val NAME = "NAME"
        val DEFAULT_NAME = "Unnamed"

        val SONG = "SONG"
        val DEFAULT_SONG = true

        val SPEED = "SPEED"
        val DEFAULT_SPEED = 0

        val COLOR = "COLOR"
        val DEFAULT_COLOR = "#3F51B5"
    }

    // NF
    private var name: String by DelegatesExt.preference(this, NAME, DEFAULT_NAME)
    private var song: Boolean by DelegatesExt.preference(this, SONG, DEFAULT_SONG)
    private var speed: Int by DelegatesExt.preference(this, SPEED, DEFAULT_SPEED)
    private var color: String by DelegatesExt.preference(this, COLOR, DEFAULT_COLOR)
    // UI
    private var toolbar: Toolbar? = null
    private var editTextName: EditText? = null
    private var switchSong: Switch? = null
    private var seekBarSpeed: SeekBar? = null
    private var buttonColor: FloatingActionButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_settings)

        // Get elements
        // UI
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        editTextName = findViewById(R.id.editTextName)
        switchSong = findViewById(R.id.switchSong)
        seekBarSpeed = findViewById(R.id.seekBarSpeed)
        buttonColor = findViewById(R.id.buttonColor)

        // Edit elements
        editTextName?.setText(name)
        switchSong?.isChecked = song
        seekBarSpeed?.progress = speed
        buttonColor?.backgroundTintList = ColorStateList.valueOf(Color.parseColor(color))

        // Refresh colors
        refreshColors()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        name = editTextName?.text.toString()
        song = switchSong?.isChecked!!
        speed = seekBarSpeed?.progress!!
        color = String.format("#%06X", 0xFFFFFF and buttonColor?.backgroundTintList?.defaultColor!!)
    }

    fun editColor(v: View) {
        ColorPickerDialog.newBuilder()
                .setDialogType(ColorPickerDialog.TYPE_PRESETS)
                .setColor(Color.parseColor(color))
                .show(this)
    }

    override fun onDialogDismissed(dialogId: Int) {}

    private fun refreshColors() {
        val intColor: Int = Color.parseColor(color)

        // ToolBar
        toolbar?.setBackgroundColor(Color.parseColor(color))

        // Status Bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) window.statusBarColor = intColor
    }

    override fun onColorSelected(dialogId: Int, color: Int) {
        buttonColor?.backgroundTintList = ColorStateList.valueOf(color)
    }
}
